import datetime
from flask import Flask
from sqlalchemy import Column, Integer, String, DateTime, LargeBinary
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

Base = declarative_base()

class ImageObject(Base):
    __tablename__ = 'images'
    id = Column(Integer, primary_key = True)
    title = Column(String(50))
    description= Column(String(55))
    urn = Column(String(15), unique=True)
    date = Column(DateTime, default=datetime.datetime.utcnow)
    picture = Column(LargeBinary)
    
    def __init__(self, title, description, urn, date, picture):
        self.title = title
        self.description = description
        self.urn = urn
        self.date = date
        self.picture = picture

engine = create_engine('postgres://sjtvztsqvwsmkz:ecf820275fb92cd929e14922b1d2b48027dc4d960c638daaf981b9a2813face4@ec2-54-225-68-71.compute-1.amazonaws.com:5432/d8nkh45i2mnjhi')

Base.metadata.create_all(engine)
        
    