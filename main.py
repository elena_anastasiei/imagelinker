import os
import uuid
import datetime
import random
import io
import base64

from flask import Flask, request, render_template, redirect, session
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from PIL import Image
from models import Base, ImageObject


app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = 'static/uploads/'
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'jpg', 'jpeg'])

app.secret_key = 'test'

engine = create_engine('postgres://sjtvztsqvwsmkz:ecf820275fb92cd929e14922b1d2b48027dc4d960c638daaf981b9a2813face4@ec2-54-225-68-71.compute-1.amazonaws.com:5432/d8nkh45i2mnjhi')

Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)

db_obj = DBSession()
    
def allowed_file(image_extention):
    return '.' in image_extention and \
           image_extention.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

def convert_image(image_bytes):
    return base64.b64encode(image_bytes)
    
def resize_image(image_file, size):
    pil_image = Image.open(io.BytesIO(image_file))
    pil_image.resize(size, Image.ANTIALIAS)
    b = io.BytesIO()
    pil_image.save(b, format='JPEG')
    image_bytes = b.getvalue()
    return convert_image(image_bytes)

    
@app.route('/', defaults={'home_image': 'static/img/placeholder.jpg'}, methods=['POST', 'GET'])
def index(home_image):
    random_image_number = random.randrange(0, db_obj.query(ImageObject).count())
    random_image_obj = db_obj.query(ImageObject)[random_image_number]
    home_image = convert_image(random_image_obj.picture)
    
    list_images = []
    all_images = db_obj.query(ImageObject).all()
    for image_obj in all_images: 
        size = 128, 128
        image_file = resize_image(image_obj.picture, size)
        list_images.append((image_obj, image_file))
    uploaded_image = {}
    visibility = ''
    
    if request.method == 'POST':
        if request.files['file']:
            file = request.files['file']
            if file and allowed_file(file.filename):
                fileuid = str(uuid.uuid4())[:8]
                picture = file.read()
                size = 200, 200
                home_image = resize_image(picture, size)
                
                image_name = request.form['image-name']
                image_description = request.form['image-description']
                image_url='https://imagelinkr.herokuapp.com/'+fileuid
                
                upload_image_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
                
                image = ImageObject(image_name,  image_description, fileuid, upload_image_date, picture)
                
                uploaded_image['date'] = upload_image_date
                uploaded_image['link'] = image_url
                uploaded_image['description'] = image_description
                uploaded_image['title'] = image_name
                
                visibility = 'display:none;'
                db_obj.add(image)
                db_obj.commit()            
    return render_template('index.html', home_image=home_image, list_images=list_images, uploaded_image=uploaded_image, visibility=visibility)

@app.route('/<urn>')
def user_image(urn):
    image_obj = db_obj.query(ImageObject).filter(ImageObject.urn == urn).first()
    size = 300, 300
    image_file = resize_image(image_obj.picture, size)
    return render_template('image.html', image_file=image_file, image_obj=image_obj)
    
@app.route('/about')
def about():
    return render_template('about.html')
    
@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/login')
def login_page():
    return render_template('login.html', message='Error')
    
@app.route('/signup')
def signup_page():
    return render_template('signup.html')

@app.route('/dosignup', methods=['POST'])
def do_signup():
    pass
    
@app.route('/dologin', methods=['POST'])
def do_login():
    email = request.form.get('email')
    password = request.form.get('password')
    
    if _do_login(email, password):
        session['email'] = email
        return redirect('/dashboard')
    else:
        return render_template('login.html', message="invalid login")
    
def _do_login(email, password):
    return True
    
@app.route('/dashboard')
def dashboard(): 
    if 'email' in session:
        return render_template('dashboard.html')     
    return redirect('/login')
    

if __name__ == '__main__':
    app.run(debug=True)